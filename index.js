const http = require("http");

const port = 4000;

http.createServer(function (req, res) {
  if (req.url === "/" && req.method === "GET" ) {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Welcome to Booking System");

  } else if (req.url === "/profile" && req.method === "GET" ) {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Welcome to your profile!");

  } else if (req.url === "/courses" && req.method === "GET" ) {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Here’s our courses available");

  } else if (req.url === "/addcourse" && req.method === "POST" ) {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Add a course to our resources");

  } else if (req.url === "/updatecourse" && req.method === "PUT" ) {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Update a course to our resources");

  } else if (req.url === "/archivecourses" && req.method === "DELETE" ) {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Archive courses to our resources");
    
  } else {
    res.writeHead(404, { "Content-Type": "text/plain" });
    res.end("404 Error: Page not found");
  }
}).listen(port)


console.log(`Server is running at localhost:${port}`);
